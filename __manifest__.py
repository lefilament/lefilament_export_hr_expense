{
    'name': 'Export Journal HR Expense',
    'summary': '''
        Export des journaux pour import outil comptable pour les frais
        ''',
    'description': '''
        Export des journaux pour import dans outils comptable pour les frais
        ''',
    'author': 'LE FILAMENT',
    'version': '12.0.1.0.0',
    'license': "GPL-3",
    'depends': ['lefilament_export_journal', 'hr_expense'],
    'qweb': [],
    'auto_install': True,
    'data': [
    ],
}
