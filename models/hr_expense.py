# Copyright 2004-2015 Odoo S.A. for original function
# (under LGPL license orginally)
# Copyright 2019 Le Filament (<http://www.le-filament.com>)
# License GPL-3 or later (http://www.gnu.org/licenses/gpl.html).

from odoo import api, models, _
from odoo.exceptions import UserError


class LeFilamentHrExpense(models.Model):
    _name = 'hr.expense'
    _inherit = 'hr.expense'

    # Modifie la fonction action_move_create de hr_expense
    # Ajoute la référence de la note de frais dans le champ
    # 'narration' de account_move
    # Modifie la variable 'aml' pour lui donner le format suivant :
    # ligne de frais - Prénom NOM

    @api.multi
    def action_move_create(self):
        '''
        main function that is called when trying to create the accounting
        entries related to an expense
        '''
        for expense in self:
            journal = (expense.sheet_id.bank_journal_id
                       if expense.payment_mode == 'company_account'
                       else expense.sheet_id.journal_id)
            # create the move that will contain the accounting entries
            acc_date = expense.sheet_id.accounting_date or expense.date
            move = self.env['account.move'].create({
                'journal_id': journal.id,
                'company_id': self.env.user.company_id.id,
                'date': acc_date,
                'ref': expense.sheet_id.name,
                'narration': expense.reference,
                # force the name to the default value, to avoid an eventual
                # 'default_name' in the context to set it to '' which cause
                # no number to be given to the account.move when posted.
                'name': '/',
            })
            company_currency = expense.company_id.currency_id
            diff_currency_p = expense.currency_id != company_currency
            # one account.move.line per expense (+taxes..)
            move_lines = expense._move_line_get()

            # create one more move line, a counterline for the total on
            # payable account
            payment_id = False
            total, total_currency, move_lines = (
                expense._compute_expense_totals(
                    company_currency, move_lines, acc_date))
            if expense.payment_mode == 'company_account':
                journal = expense.sheet_id.bank_journal_id
                if not journal.default_credit_account_id:
                    raise UserError(
                        _("No credit account found for the %s journal, \
                          please configure one.")
                        % (journal.name))
                emp_account = journal.default_credit_account_id.id
                # create payment
                payment_methods = (
                    journal.outbound_payment_method_ids if total < 0
                    else journal.inbound_payment_method_ids)
                journal_currency = (journal.currency_id
                                    or journal.company_id.currency_id)
                payment = self.env['account.payment'].create({
                    'payment_method_id': (payment_methods[0].id
                                          if payment_methods else False),
                    'payment_type': total < 0 and 'outbound' or 'inbound',
                    'partner_id': (expense.employee_id.address_home_id
                                   .commercial_partner_id.id),
                    'partner_type': 'supplier',
                    'journal_id': journal.id,
                    'payment_date': expense.date,
                    'state': 'reconciled',
                    'currency_id': (expense.currency_id.id if diff_currency_p
                                    else journal_currency.id),
                    'amount': (abs(total_currency) if diff_currency_p
                               else abs(total)),
                    'name': expense.name,
                })
                payment_id = payment.id
            else:
                if not expense.employee_id.address_home_id:
                    raise UserError(
                        _("No Home Address found for the employee %s, \
                          please configure one.") % (expense.employee_id.name))
                emp_account = expense.employee_id.address_home_id\
                    .property_account_payable_id.id

            aml_name = (expense.name.split('\n')[0][:64]
                        + ' - ' + expense.employee_id.name)
            move_lines.append({
                'type': 'dest',
                'name': aml_name,
                'price': total,
                'account_id': emp_account,
                'date_maturity': acc_date,
                'amount_currency': (total_currency if diff_currency_p
                                    else False),
                'currency_id': (expense.currency_id.id if diff_currency_p
                                else False),
                'payment_id': payment_id,
                })

            # convert eml into an osv-valid format
            lines = [(0, 0, expense._prepare_move_line(x)) for x in move_lines]
            move.with_context(
                dont_create_taxes=True).write({'line_ids': lines})
            expense.sheet_id.write({'account_move_id': move.id})
            move.post()
            if expense.payment_mode == 'company_account':
                expense.sheet_id.paid_expense_sheets()
        return True
